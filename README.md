# Sistema de Controle de Servidores - API

_Projeto desenvolvido como teste para vaga Java_

O Sistema de Controle de Servidores foi desenvolvido para armazenamento, busca e detalhamento de servidores da empresa.


# Entrega do Teste

Disponibilizar um repositório público em seu GitLab e nos passar a URL.

Se fizer um _fork_ será desclassificado!

**Requerimentos para Execução:**

JDK 1.8 - http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

Mongo - https://docs.mongodb.com/manual/administration/install-community/


### O Desafio

Seu objetivo é criar uma API simples rotas a partir de um banco de dados mongo (local) chamado _servidores_

* id : string (Gerado automaticamente pelo mongo)  (Obrigatório)
* nomeServidor: string (Obrigatório)
* dataCriacao (Gerado a partir de um /post) (Obrigatório)
* usuarioInclusao: string (Obrigatório)
* descricao: string
* sistemaOperacional: string (Obrigatório)


API com as seguintes rotas
-------------------

GET   /servidor

GET   /servidor/{id}

POST  /servidor

PUT   /servidor/{id}


**Exemplos de Retornos/Entradas JSON**

Servidor JSON:

```json

    {
      "id": "5bbb7c37a3271d00011f5027",
      "nomeServidor": "Servior01",
      "dataCriacao" : 1539013645480,
      "usuarioInclusao" : "Fulano da Silva",
      "descricao" : "Este Servidor tem arquivos de backup",
      "sistemaOperacional" : "linux"
    }
```

###(PLUS)
 - Testes, no mínimo testes unitários 
 - Padrão de Projeto e boas práticas de Orientação a Objetos;
    

 


 